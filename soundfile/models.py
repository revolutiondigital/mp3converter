from django.db import models

def upload_job_file_path(instance, filename):
    return 'uploads/job_meta/files/%s' % (filename)


class JobFileSubmit(models.Model):
    file = models.FileField(upload_to=upload_job_file_path, blank=False, null=False)
    uploadDate = models.DateTimeField(auto_now=True)