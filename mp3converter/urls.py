from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
admin.autodiscover()
handler404 = 'mp3converter.views.handle_404'
handler404 = 'mp3converter.views.handle_500'
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
		(r'^admin/', include(admin.site.urls)),
	 url(r'^$', 'mp3converter.views.home', name='home'),
     url(r'^robots\.txt$', 'mp3converter.views.robotstxt'),
     url(r'^BingSiteAuth\.xml$', 'mp3converter.views.bingsiteauth'),
     url(r'^privacy-policy$', 'mp3converter.views.privacypolicy'),
	 url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    # Examples:
    # url(r'^$', 'mp3converter.views.home', name='home'),
    # url(r'^mp3converter/', include('mp3converter.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
