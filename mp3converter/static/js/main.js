
//allow upload
allow_upload = false;

$('#convert_file').change(function(){
	stringname = document.getElementById('convert_file').value
	
	if (stringname === ""){
		return
	}
	value = stringname.substr(stringname.length - 3);

	// check to see if the filetype is allowed
	if (value.indexOf("mp3") == -1 && value.indexOf("wav") == -1 && 
		value.indexOf("m4a") == -1 && value.indexOf("ogg") == -1 ){

		$('.select-file-upload').hide()
		//show upload type
		$('.file-declined').show()
		$('.show-file-name-error').append(filename = $('input[type=file]').val().split('\\').pop())
		$('.file-declined').addClass('animated fadeInDown');
		allow_upload = false;
		return
	}

	document.getElementById('convert_from').value  = value
	
	$('.hide-convert').hide()

	//firefox has no fakepath
	if (stringname.substr(stringname.lastIndexOf('fakepath')) == 3){
		filename = stringname;
	}
	else{
		filename = stringname.substr(stringname.lastIndexOf('fakepath')+9);
	}	
	$('.show-file-name').append(filename = $('input[type=file]').val().split('\\').pop())
	//hide upload button
	$('.select-file-upload').addClass('animated fadeOut');
	$('.select-file-upload').hide()
	//show upload type
	$('.file-accepted').show()
	$('.file-accepted').addClass('animated fadeInDown');
	//allow the upload to proceed
	allow_upload = true;
	//change the buttons to disable one with file type
	hideButton(value)
	//remove disabled on convert to
	$( ".file-selector-btn" ).each(function( index ) {
	  $( this ).removeAttr('disabled')
	});
	document.getElementById('convert_to').value  = ""


$("#convert_to").val($("#convert_to option:first").val());

})

//hide button that the file is
function hideButton(filetype){
if(filetype === "mp3"){
	return
}else{
$( ".file-selector-btn" ).each(function( index ) {
	  $( this ).removeClass('btn-primary')
	  $( this ).show()
	});
$('#'+filetype).hide()
}
}
//FUNCTION
//handle filetype button click
$('.file-selector-btn').click(function(){
	//make sure button is disabled
	$('.convert-button').attr('disabled',true)
	$( ".file-selector-btn" ).each(function( index ) {
	  $( this ).removeClass('btn-primary')
	});
	$(this).addClass('btn-primary')
	document.getElementById('convert_to').value  = this.id
	
	if (this.id === "wav"){
		$('.quality-bitrate').addClass('animated fadeOut');
		$('.quality-bitrate').hide()
		

	}
	//if you can select bit rate
	else{
		$('.quality-bitrate').show()
		$('.quality-bitrate').removeClass('animated fadeOut');
		$('.quality-bitrate').addClass('animated fadeIn');
		$( ".quality-selector-btn" ).each(function( index ) {
	  	$( this ).removeAttr('disabled')
		});
		//set default regular bitrate
		$('#128').addClass('btn-primary')
		document.getElementById('bitrate_quality').value  = "128"


	}

	$('.convert-button').removeAttr('disabled')

})
//FUNCTION
//CHANGE BITRATE
$('.quality-selector-btn').click(function(){
	$( ".quality-selector-btn" ).each(function( index ) {
		  $( this ).removeClass('btn-primary')
		  $( this ).show()
		});
		$(this).addClass('btn-primary')
	document.getElementById('bitrate_quality').value  = this.id
})
//FUNCTION
//function to hide box if wrong file is selected
$('.not-this-file').click(function(){
	//clear filetype
	 $('.file-accepted').hide()
	 $('.select-file-upload').removeClass('animated fadeInDown');
	 $('.file-declined').hide()
	 $('.select-file-upload').removeClass('animated fadeInDown');
	 $('.show-file-name-error').html("")
	 $('.show-file-name').html("")
	 allow_upload= false
	 $('.select-file-upload').show()
	 //show upload file again
	  $('.select-file-upload').removeClass('animated fadeOut');
	  $('.select-file-upload').addClass('animated fadeIn');

	  $("#convert_file").val("")
	  //show buttons again and disable
	  $( ".file-selector-btn" ).each(function( index ) {
	  	$(this).show()
	  $( this ).attr('disabled', true)
	  $( this ).removeClass('btn-primary')
	});

	  //show quality and disable
	  $('.quality-bitrate').removeClass('animated fadeOut');
	  $('.quality-bitrate').addClass('animated fadeIn');
	  
	  $( ".quality-selector-btn" ).each(function( index ) {
	  $( this ).attr('disabled', true)
	  $( this ).removeClass('btn-primary')
	});
	  //remove all values from hidden inputs
	  	$('#convert_from').val = ""
		$('#convert_to').val = ""
		$('#bitrate_quality').val = ""

})

$('.upload-button').click(function(){

	$('#convert_file').click()

})

//do all checks when you convert
$('#convert_button').click(function(){
	$('.submit_it').submit()
	$('.overlay').show()
	$('.overlay').addClass('animated fadeIn')
})

//FUNCTION
//dot dot dot for converting files
var dots = window.setInterval( function() {
    var wait = document.getElementById("wait");
    if ( wait.innerHTML.length > 3 ) 
        wait.innerHTML = "";
    else 
        wait.innerHTML += ".";
    }, 1000);
