
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
import os
from soundfile.models import *
import subprocess
def handle_404(request):
	return render_to_response("404.html",form_args, context_instance=RequestContext(request))
def handle_500(request):
	return render_to_response("500.html",form_args, context_instance=RequestContext(request))	
def robotstxt(request):
	return render_to_response("robots.txt", context_instance=RequestContext(request))
def bingsiteauth(request):
	return render_to_response("BingSiteAuth.xml", context_instance=RequestContext(request))			
def privacypolicy(request):
	return render_to_response("privacypolicy.html", context_instance=RequestContext(request))
def home(request):
	if request.method == "GET":
		form_args={}
		return render_to_response("home.html",form_args, context_instance=RequestContext(request))
	else:
		form_args = {}
		data = request.FILES.get('convert_file')
		data_string = str(data).replace(' ','')

		
		convert_from = request.POST.get('convert_from')
		convert_to=request.POST.get('convert_to')
		path = default_storage.save('tmp/' +data_string , ContentFile(data.read()))

		#get path length to append to file type
		print path
		path_length = len(str(path))
		tmp_file_path = os.path.join(settings.MEDIA_ROOT, path)
		length_file_path = len(str(tmp_file_path))
		#convert to wav
		if convert_to == "wav":
			file_from = str(tmp_file_path)
			wav = str(tmp_file_path[:length_file_path -4]) + '.wav'
			new_path = str(path[:path_length -4]) + '.wav'
			cmd = "ffmpeg -i  '%s' -y -acodec pcm_s16le '%s'" % (file_from,wav)
			subprocess.call(cmd, shell=True)
			os.remove(file_from)	
			form_args['file']= new_path
		#convert to mp3, wma
		if  convert_to == "mp3":
			#get bitrate
			bitrate = request.POST.get('bitrate_quality')
			file_from = str(tmp_file_path)
			mp3 = str(tmp_file_path[:length_file_path -4])+'.mp3'
			#if convert wav = mp3
			if (convert_from == "wav"):
				cmd =  "lame --preset insane -b%s  '%s' " % (bitrate,file_from)
				new_path = str(path[:path_length -4]) + '.mp3'
			elif (convert_from =="mp3"):
				mp3 = str(tmp_file_path[:length_file_path -5])+'_converted.mp3'
				cmd =  "lame  -b%s  '%s' '%s'" % (bitrate,file_from,mp3)
				new_path = str(path[:path_length -5])+'_converted.mp3'
			else:
				cmd =  "ffmpeg -i '%s' -y -acodec mp3 -ab '%sk'  '%s'" % (file_from,bitrate,mp3)
				new_path = str(path[:path_length -4]) + '.mp3'	
			
			subprocess.call(cmd, shell=True)
			os.remove(file_from)	
			form_args['file'] = new_path
		#convert to wma	
		if convert_to =="wma" or convert_to == "ogg" or convert_to == "m4a":
			
			bitrate = request.POST.get('bitrate_quality')
			file_from = str(tmp_file_path)
			file_to = str(tmp_file_path[:length_file_path -4])+'.'+convert_to
			if convert_to == "ogg":
				cmd =  "ffmpeg -i '%s' -y -c:a libvorbis -ab '%sk'  '%s'" % (file_from,bitrate,file_to)	
			elif convert_to == "wma":	
				cmd =  "ffmpeg -i '%s' -y -ab '%sk'  '%s'" % (file_from,bitrate,file_to)	
			elif convert_to == "m4a":	
				cmd =  "ffmpeg -i '%s' -y -ab '%sk' -c:a libfaac -vn '%s'" % (file_from,bitrate,file_to)		
			new_path = str(path[:path_length -4]) + '.'+convert_to
			subprocess.call(cmd, shell=True)
			os.remove(file_from)	
			form_args['file'] = new_path			
		#sound = AudioSegment.from_mp3(newfile.file)
		#sound.export(tmp_file_path+"/converted", format="wav")
		filename  = new_path[4:]
		form_args['filename']= filename
		print form_args
		return render_to_response("home.html", form_args,context_instance=RequestContext(request))	


