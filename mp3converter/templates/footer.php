<div id="footer">
	<div class = "footer-container">

		<div id="products-container">
			<div class = "products">
				<a href="http://matheasysolutions.com/calculators" class = "products-title">Calculators</a>
				<ul class = "products-list">
					<li><a href='http://bmicalculator.cc'>BMI Calculator</a></li>
		            <li><a href='http://finalexamcalculator.com'>Final Grade Calculator</a></li>
		            <li><a href='http://gpacalculator.cc'>GPA Calculator</a></li>
		            <li><a href='http://grade-calculator.com'>Grade Calculator</a></li>
		            <li><a href='http://percentcalculator.com'>Percentage Calculator</a></li>
			    </ul>
			</div>

			<div class = "products">
				<a class = "products-title">&nbsp;</a>
				<ul class = "products-list">
		            <li><a href='http://vatcalculator.co'>VAT Calculator</a></li>
		            <li><a href='http://finalexamcalculator.com/midterm'>Midterm Calculator</a></li>
					<li><a href='http://finalexamcalculator.com/youtube'>YouTube RPM Calculator</a></li>
					<li><a href='http://finalexamcalculator.com.br'>Calculadora Nota Final</a></li>
			    </ul>
			</div>

			<div class = "products">
			   	<a href="http://matheasysolutions.com/mobile-apps" class = "products-title">Mobile Apps</a>
				<ul class = "products-list">
					<li><a href = "https://itunes.apple.com/ca/app/fail-safe-final-exam-calculator/id673084580">Final Grade Calculator (iPhone)</a></li>
					<li><a href = "http://matheasysolutions.com/mobile-apps/hashtag-everything">Hashtag Everything</a></li>
					<li><a href = "http://dogebounce.com">Doge Bounce (iPhone)</a></li>
					<li><a href = "https://play.google.com/store/apps/details?id=com.dogetile.android">Doge Tile (Android)</a></li>
			    </ul>
			</div>

			<div class = "products">
			   	<a href="http://matheasysolutions.com/partners" class = "products-title">Partners</a>
				<ul class = "products-list">
					<li><a href = "http://chinchatcomics.com/" title="Chinchilla Comics Galore">Chin Chat Comics</a></li>
			    </ul>
			</div>
		</div>


		<span>
			<a href="mailto:math.easy.solutions@gmail.com">Contact Us</a> | <a href="/privacy-policy">Privacy Policy</a> | <a href="http://matheasysolutions.com/donate">Donate</a>
		</span>
		<span class = "extra-info">Copyright &copy; 2014 <a href="http://matheasysolutions.com">Math Easy Solutions</a></span>

	</div>
</div>



