<div id="header">
	<div id="header-top">
		<div id="logo-container">
			<div id="logo-image-container">
				<a href='/'>
					<img alt = "MP3 Converter by Math Easy Solutions" src = "http://finalexamcalculator.com/images/mp3-converter-icon.png" >
				</a>
			</div>

			<div id="logo-text-container">
				<div class="bubble">
					<a href='/'>
						<p id="calculator-title">MP3 Converter</p>
					</a>
					<p id="tag-line">Convert YouTube videos, WAV, and more to MP3.</p>
					<!--<p id="powered-by-mes">Powered by <a href='http://matheasysolutions.com'><span>Math Easy Solutions</span></a></p>-->
				</div>
			</div>
		</div>

		<div id="social-media-container">
			<div id="social-media-links">
				<table>
					<tr>
						<td><a href="#" target="_blank">
							<img src="http://finalexamcalculator.com/images/facebook-icon.png" alt="facebook icon" class="social-media-button"></a></td>
						<td><a href="#" target="_blank">
							<img src="http://finalexamcalculator.com/images/twitter-icon.png" alt="twitter icon" class="social-media-button"></a></td>
						<td><a href="#" rel="publisher" target="_blank">
							<img src="http://finalexamcalculator.com/images/google-plus-icon.png" alt="google plus icon" class="social-media-button"></a></td>
					</tr>				
				</table>
			</div>

			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_native_toolbox" data-url="http://mp3converter.ws" data-title="MP3 Converter by Math Easy Solutions"></div>
		</div>
	</div>


	<div class="line"></div>

	<div id="navbar">
		<ul>
			<li class="calc-tab"><a href='/'>MP3 Converter</a></li>
            <li class="tutorial-tab"><a href='#'>Video Tutorial</a></li>
            <li class="other-calcs-tab"><a href="http://matheasysolutions.com/calculators/">Calculators</a>
                <ul>
                	<li id="bmi-tab"><a href='http://bmicalculator.cc'>BMI Calculator</a></li>
                    <li id="fec-tab"><a href='http://finalexamcalculator.com'>Inflation Calculator</a></li>
                    <li id="gpa-tab"><a href='http://gpacalculator.cc'>GPA Calculator</a></li>
                    <li id="grade-tab"><a href='http://grade-calculator.com'>Grade Calculator</a></li>
                    <li id="pc-tab"><a href='http://percentcalculator.com'>Percentage Calculator</a></li>
                    <li id="vat-tab"><a href='http://vatcalculator.co'>VAT Calculator</a></li>
                </ul>
            </li>          
            <li><a class="mes-tab" href='#'>Math Easy Solutions</a>
                <ul class="hover">
                    <li><a href='http://matheasysolutions.com/calculators'>More Calculators</a></li>
                    <li><a href='http://matheasysolutions.com/mobile-apps'>Mobile Apps</a></li>
                    <li><a href='http://matheasysolutions.com/videos'>Math Tutorials</a></li>
                    <li><a href='http://matheasysolutions.com/memes'>Math Memes</a></li>
                    <li><a href='http://matheasysolutions.com/partners'>Partners</a></li>
                </ul>
            </li>
      	</ul>
	</div>

</div>