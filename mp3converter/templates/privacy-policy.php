<!DOCTYPE html>
<html>
	<head>
		<title>Privacy Policy | MP3 Converter</title>
		<!--<link rel="icon" type="image/png" href="">-->
		<link rel='stylesheet' type='text/css' href='css-reset.css'/>
		<link rel='stylesheet' type='text/css' href='main.css'/>

        <style>
        #main-content p {line-height:2;font-weight: 100;margin-top:1.3em;}
        #main-content .small-margin {margin-top:0.5em;}
        #main-content a {text-decoration: underline;color: -webkit-link;}
        #main-content .mini-title {line-height:2;font-weight: 700;margin-top: 1.3em;}
        #main-content .generated-by {font-size: 0.8em;}
        </style>
		
	    <meta charset="utf-8">
		<!--<meta name="viewport" content="width=device-width; initial-scale=1.0">-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<?php include "google-analytics.php"; ?>
	</head>

	<body>	
		<?php include "fb-sdk.php"; ?>

		<div id = "outer-container">
			<div id = "main-container">
				<div class = "inner-container">
					<?php 
					include "header.php";
					//include "big-ads.php";
					?>

					<div id = "page-content">
						<div id="inner-content">
							<div id="page-title-small-container">
								<div class="color-box"></div>
								<h1 id="page-title-small">Privacy Policy</h1>
							</div>
							<div class="small-line"></div>

							<div id="main-content">

								<p>
								This Privacy Policy governs the manner in which MP3 Converter collects, uses, maintains and
								discloses information collected from users (each, a "User") of the mp3converter.ws website
								("Site"). This privacy policy applies to the Site and all products and services offered by MP3 Converter.
								</p>

								<h2 class="mini-title">Personal identification information</h2>
								
								<p>
								We may collect personal identification information from Users in a variety of ways, including, but
								not limited to, when Users visit our site, register on the site, place an order, subscribe to the
								newsletter, respond to a survey, fill out a form, and in connection with other activities, services,
								features or resources we make available on our Site. Users may be asked for, as appropriate, name,
								email address. Users may, however, visit our Site anonymously. We will collect personal
								identification information from Users only if they voluntarily submit such information to us. Users
								can always refuse to supply personally identification information, except that it may prevent them
								from engaging in certain Site related activities.
								</p>
								
								<h2 class="mini-title">Non-personal identification information</h2>
								
								<p>
								We may collect non-personal identification information about Users whenever they interact with our
								Site. Non-personal identification information may include the browser name, the type of computer and
								technical information about Users means of connection to our Site, such as the operating system and
								the Internet service providers utilized and other similar information.
								</p>
								
								<h2 class="mini-title">Web browser cookies</h2>
								
								<p>
								Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their
								hard drive for record-keeping purposes and sometimes to track information about them. User may
								choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If
								they do so, note that some parts of the Site may not function properly.
								</p>
								
								<h2 class="mini-title">How we use collected information</h2>
								
								<p>
								MP3 Converter may collect and use Users personal information for the following purposes:
								</p>
								
								<p class="italic">
								- To improve customer service
								</p>
								<p class="small-margin">Information you provide helps us respond to your customer service requests and support needs more efficiently. 
								<p class="italic">- To personalize user experience</p>
								<p class="small-margin">We may use information in the aggregate to understand how our Users as a group use the services and resources
								provided on our Site. </p>
								<p class="italic">- To improve our Site</p>
								<p class="small-margin">We may use feedback you provide to improve our products and services.</p>
								<p class="italic">- To process payments</p>
								<p class="small-margin">We may use the information Users provide about themselves when placing an order only to provide 
								service to that order. We do not share this information with outside parties except to the extent 
								necessary to provide the service.</p>
								<p class="italic">- To run a promotion, contest, survey or other Site feature</p>
								<p class="small-margin">To send Users information they agreed to receive about topics we think will be of interest to them.
								<p class="italic">- To send periodic emails</p>
								<p class="small-margin">We may use the email address to send User information and updates 
								pertaining to their order. It may also be used to respond to their inquiries, questions, 
								and/or other requests. If User decides to opt-in to our mailing list, they will receive 
								emails that may include company news, updates, related product or service information, etc.
								If at any time the User would like to unsubscribe from receiving future emails, 
								we include detailed unsubscribe instructions at the bottom of each email or 
								User may contact us via our Site.
								</p>
								
								<h2 class="mini-title">How we protect your information</h2>
								
								<p>
								We adopt appropriate data collection, storage and processing practices and security measures to
								protect against unauthorized access, alteration, disclosure or destruction of your personal
								information, username, password, transaction information and data stored on our Site.
								</p>
								
								<h2 class="mini-title">Sharing your personal information</h2>
								
								<p>
								We do not sell, trade, or rent Users personal identification information to others. We may share
								generic aggregated demographic information not linked to any personal identification information
								regarding visitors and users with our business partners, trusted affiliates and advertisers for the
								purposes outlined above.
								</p>
								
								<h2 class="mini-title">Third party websites</h2>
								
								<p>
								Users may find advertising or other content on our Site that link to the sites and services of our
								partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the
								content or links that appear on these sites and are not responsible for the practices employed by
								websites linked to or from our Site. In addition, these sites or services, including their content
								and links, may be constantly changing. These sites and services may have their own privacy policies
								and customer service policies. Browsing and interaction on any other website, including websites
								which have a link to our Site, is subject to that website's own terms and policies.
								</p>
								
								<h2 class="mini-title">Advertising</h2>
								
								<p>
								Ads appearing on our site may be delivered to Users by advertising partners, who may set cookies.
								These cookies allow the ad server to recognize your computer each time they send you an online
								advertisement to compile non personal identification information about you or others who use your
								computer. This information allows ad networks to, among other things, deliver targeted
								advertisements that they believe will be of most interest to you. This privacy policy does not cover
								the use of cookies by any advertisers.
								</p>
								
								<h2 class="mini-title">Google Adsense</h2>
								
								<p>
								Some of the ads may be served by Google. Google's use of the DART cookie enables it to serve ads to
								Users based on their visit to our Site and other sites on the Internet. DART uses "non personally
								identifiable information" and does NOT track personal information about you, such as your name,
								email address, physical address, etc. You may opt out of the use of the DART cookie by visiting the
								Google ad and content network privacy policy at http://www.google.com/privacy_ads.html
								</p>
								
								<h2 class="mini-title">Changes to this privacy policy</h2>
								
								<p>
								MP3 Converter has the discretion to update this privacy policy at any time. When we do, we will
								revise the updated date at the bottom of this page. We encourage Users to frequently check this page
								for any changes to stay informed about how we are helping to protect the personal information we
								collect. You acknowledge and agree that it is your responsibility to review this privacy policy
								periodically and become aware of modifications.
								</p>
								
								<h2 class="mini-title">Your acceptance of these terms</h2>
								
								<p>
								By using this Site, you signify your acceptance of this policy. If you do not agree to this policy,
								please do not use our Site. Your continued use of the Site following the posting of changes to this
								policy will be deemed your acceptance of those changes.
								</p>
								
								<h2 class="mini-title">Contacting us</h2>
								
								<p>
								If you have any questions about this Privacy Policy, the practices of this site, or your dealings
								with this site, please contact us at: what-is-mp3's-mail?@gmail.com
								</p>
								
								<p>
								This document was last updated on July 22, 2014
								</p>

								<p class="generated-by">
								Privacy policy created by http://www.generateprivacypolicy.com
								</p>
							</div>
							<?php //include "small-ads.php"; ?>
							<div class="fb-comments" data-href="http://mp3converter.ws/privacy-policy" data-width="100%" data-numposts="10" data-colorscheme="light" data-order-by="reverse_time"></div>

						</div>
					</div>

				</div>
				<?php include "footer.php"; ?>
			</div>
		</div>
		<script>$("#navbar .calc-tab").addClass("active-tab");</script>

		<script src="calculator.js"></script>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53b7585d09d9e0bd"></script>
	</body>
</html>