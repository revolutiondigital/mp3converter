<!DOCTYPE html>
<html>
	<head>
		<title>MP3 Converter | Math Easy Solutions</title>
		<link rel="icon" type="image/png" href="">
		<link rel='stylesheet' type='text/css' href='{{STATIC_URL}}css/css-reset.css'/>

		<link rel='stylesheet' type='text/css' href='{{STATIC_URL}}css/page-content.css'/>+
		<link href="{{STATIC_URL}}css/bootstrap.min.css" rel="stylesheet">
     <link href="{{STATIC_URL}}css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{STATIC_URL}}css/landing-page.css" rel="stylesheet">
     <link href="{{STATIC_URL}}css/main.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

	    <!--
        <link rel="author" href="https://plus.google.com/+FinalExamCalculatorPage">    
	    <meta charset="utf-8">
		<!-<meta name="viewport" content="width=device-width; initial-scale=1.0">->

        <meta property="fb:app_id"          content="292273677511642" /> 
        <meta property="og:type"            content="website" /> 
        <meta property="og:url"             content="http://finalexamcalculator.com" /> 
        <meta property="og:title"           content="MP3 Converter by Math Easy Solutions" /> 
        <meta property="og:image"           content="http://finalexamcalculator.com/images/final-grade-calculator-icon-big.png" /> 
        <meta property="og:description"     content="Use this MP3 Converter to calculate what you need on your exam to get a desired grade in the course!" /> 

        <meta name="description" content="Use this MP3 Converter to calculate what you need on your exam to get a desired grade in the course!">
        <meta name="keywords" content="MP3 Converter, Final Exam Calculator, Math Easy Solutions">-->
        <?php include "google-analytics.php"; ?>
	</head>

	<body>
		<?php include "fb-sdk.php"; ?>

		<div id = "outer-container">
			<div id = "main-container">
				<div class = "inner-container">
					<?php 
					include "header.php";
					//include "big-ads.php";
					?>
					<div id = "page-content">
						<div id="inner-content">
							<?php
							include "page-content.php";
							//include "small-ads-main.php";
							?>
							<div class="fb-comments" data-href="http://mp3converter.ws" data-width="100%" data-numposts="10" data-colorscheme="light" data-order-by="reverse_time"></div>
						</div>
					</div>
				</div>
				<?php include "footer.php"; ?>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script>$("#navbar .calc-tab").addClass("active-tab");</script>

		  <script src="{{STATIC_URL}}js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{STATIC_URL}}js/bootstrap.min.js"></script>
     <script src="{{STATIC_URL}}js/main.js"></script>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53b7585d09d9e0bd"></script>
	</body>
</html>